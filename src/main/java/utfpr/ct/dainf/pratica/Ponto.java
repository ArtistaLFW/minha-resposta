package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;
    
    public Ponto(){
        
    }
    
    public Ponto(double x,double y, double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }
    
    public double getX(){
        return x;
    }
    
    public void setX(double x){
        this.x=x;
    }
    
    public double getY(){
        return y;
    }
    
    public void setY(double y){
        this.y=y;
    }
    
    public double getZ(){
        return z;
    }
    
    public void setZ(double z){
        this.z=z;
    }
    
    
    
    
    @Override
    public boolean equals(Object o) {
 
        // If the object is compared with itself then return true  
        if (o == this) {
            return true;
        }
 
        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof Ponto)) {
            return false;
        }
         
        // typecast o to Complex so that we can compare data members 
        Ponto p = (Ponto) o;
         
        // Compare the data members and return accordingly 
        return Double.compare(x, p.x) == 0
                && Double.compare(y, p.y) == 0
                && Double.compare(z, p.z) ==0;
    }
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

}
